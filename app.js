const URL = "https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=aaa12b44c85a49df09bc449ec1f6ac82&hash=711dcb992a4dcdfcb9291bac3ab104e3"

let characters = [];


const fetchCharacters = async (url = URL) => {
    try {
        
        const response = await fetch(url);
        const { data: { results: characters } } = await response.json();
        console.log(characters);
        return characters;
    } catch (error) {
        console.error("error 404")
    }

}

const createNode = ({ id, name, thumbnail, urls }) => {

  
    const node = `
    
        <div class="col-md-4 col-12" id="${id}">
            <div class="card mt-5 ml-3 justify-content-center">
                <div class="card-body mt-2">
                    <img src="${thumbnail.path}/portrait_uncanny.${thumbnail.extension}" class="img-thumbnail"/>
                    <h4 class="card-title text-center mt-2">${name}</h5>
                    <div class="card-title text-center">
                        <a href="${urls[0].url}" target="blank" class="opacity">Mas Sobre MI</a>  
                    </div>
                    <div class="d-flex justify-content-center" >
                        <button onclick=del(${id}) class="btn btn-primary btn-block">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    `;
    document.getElementById("api").insertAdjacentHTML("beforeend", node);
};

const searchCharacter = async () => {

     mostrarCharacter();

     try {  
        const name = document.getElementById("input").value.trim();
        const hero = encodeURIComponent(name);
        const URL = `https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=${hero}&ts=1&apikey=aaa12b44c85a49df09bc449ec1f6ac82&hash=711dcb992a4dcdfcb9291bac3ab104e3`;
        const response = await fetch(URL);
        const { data: { results : characters } } = await response.json();
        iterateCharacters(characters);
        console.log(characters);
        
    }
    catch(err) {
        console.error(err);
    }    

};

const iterateCharacters = (characters) => {
    characters.map((character) => createNode(character));
}


const mostrarCharacter = () => {
    characters = [];
    document.querySelector("#api").innerHTML = '';
    const nodo =`<p id="mensaje"></p>`;
    document.querySelector("#api").insertAdjacentHTML("beforeend",nodo);
}

const showMessage = () => {
    document.getElementById('mensaje').innerHTML = "No hay mas personajes.";
    document.querySelector('#input').disabled = true;
};

const del = (id) => {
    document.getElementById(id).remove();
    characters = characters.filter(character => character.id != id);
    characters.length === 0 ? showMessage() : null;
};


async function start() {
    document.querySelector("#find").addEventListener("click", searchCharacter);
    characters = await fetchCharacters();
    iterateCharacters(characters);

}



window.onload = start();